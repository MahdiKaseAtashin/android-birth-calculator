package com.mahdikaseatashin.birthcalculator

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnDateSelector.setOnClickListener { view ->
            onClickDateSelector(view)
        }
        languageSelector.setOnClickListener { view ->
            onClickDateSelector(view)
        }
    }

    private fun onClickDateSelector(view: View) {
        when (view.id) {
            R.id.btnDateSelector -> {
                val myCalendar = Calendar.getInstance()
                val year = myCalendar.get(Calendar.YEAR)
                val month = myCalendar.get(Calendar.MONTH)
                val dayOfMonth = myCalendar.get(Calendar.DAY_OF_MONTH)
                val dataPickerDialog = DatePickerDialog(
                    this,
                    DatePickerDialog.OnDateSetListener { view, selectedYear, selectedMonth, selectedDayOfMonth ->
                        val selectedDate = "$selectedYear/${selectedMonth + 1}/$selectedDayOfMonth"

                        textViewSelectedDate.text = selectedDate

                        val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
                        val theDate = sdf.parse(selectedDate)
                        val birthInMinutes = theDate!!.time / 60000
                        val currentDate = sdf.parse(sdf.format(System.currentTimeMillis()))
                        val currentDateInMinutes = currentDate!!.time / 60000
                        textViewAgeInMinutes.text = (currentDateInMinutes - birthInMinutes).toString()
                    }, year, month, dayOfMonth
                )
                dataPickerDialog.datePicker.maxDate = Date().time - 86400000
                dataPickerDialog.show()
            }
            R.id.languageSelector -> {

            }
        }
    }
}
